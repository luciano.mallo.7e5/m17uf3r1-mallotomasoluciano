# Survivor 3D

## Player i Càmera

- Carles Carrasco Córdoba
- Eduard Ruesga Cortiella

- Format d'entrega:
  - Repositori a PUBLIC a Gitlab amb el següent format de nom de projecte: M17UF2R1-CognomsNom.
  - Mínim dues branques: Main i Dev. Només es corregirà el commit de la branca Main. Aquest ha de ser funcional: que es pugui executar (botó play).
  - Nom del commit d'entrega: "R1.1-Entrega"
  - Readme: Instruccions de joc: tecles d'execució per a cada escena si es cau.
  - El projecte de Unity ha d'estar endreçat i amb les escenes correctament nomenades segons enunciat i dins de la build.

---

### Per a la realització d’aquest repte construirem el player del nostre joc. Hi ha dues parts:

- PlayerTest: on el nostre personatge cobra vida amb animacions i el podem controlar
- Camera setting: configurarem la càmera que seguirà al player.

La intenció del joc general és fer un petit món 3D on el nostre personatge necessitarà evitar o guanyar enemics mentre compleix/resolt un petit puzle o col·lecció d’objectes. Les mecàniques de progressió de joc i la temàtica queden a la vostra elecció. És important que s'adeqüin als requisits dels reptes.

Recomanació: centreu-vos a fer els punts del repte que són amb els que realment puntuareu. Després podeu dedicar més a l’ambientació i detalls.

Com en altres reptes certs punts extres es poden canviar per altres desenvolupaments similars. Abans de fer-ho consulteu al docent.

---

### R1.1.1 - Player Test: Moviment i animacions

    Per realitzar aquesta part creareu una escena amb un terra, alguns obstacles, una càmera fixa i el nostre personatge. L'escena tindrà nom de "Test-R1.1.1"
    Importar un model de tipus humanoide que serà el protagonista del nostre joc. 
    Fer servir models ja creats, per exemple el que s’esmenta a l’apartat de Recursos.
    Importar correctament materials i textures. Si no té el rigging fet recordeu el recurs de mixamo o blender.

#### Verbs que ha de tenir el personatge:

- Idle: repòs
- Caminar dret
- Córrer dret
- Disparar: el protagonista portarà una arma i ha de poder disparar mentre està quiet, caminant o saltant.
  - No pot disparar mentre va ajupit o roda cap endavant **(extra)**.
- Afegir animation layer per afegir l’estat d’apuntar amb l’arma mentres camina. En aquest punt, treure i guardar la pistola es poden fer utilitzant transicions entre estats de l’Animator.
- Ajupir-se/caminar ajupit/aixecar-se: no pot disparar mentre es fan aquestes accions.
- Saltar: saltarà sempre una mida fixa.
- Agafar/utilitzar: animació genèrica que emprarem per interactuar amb elements de l'escena. Exemples: obrir portes, agafar objectes, activar palanques.
- Celebració: animació on es perd el control del personatge fins que acaba.

- **(extra)** Idle avançat: el personatge realitza diferents animacions depenent del temps d'estat de repòs.
- **(extra)** Rolling: acció de tombarella cap endavant que es realitza mentres s'està ajupit. Es pot fer directament "rolling" quan s'està dret.
- **(extra)** Saltar: salt de mida variable segons script.
- **(extra)** Caminar/córrer ferit
- **(extra)** Treure i guardar arma: una animació feta adhoc de desenfundar i guardar l’arma. D’aquesta forma s’afina molt millor el verb de disparar.

#### Crear l'animator adient per a l'execució de les animacions dels verbs del personatge

- Ha de tenir mínim 1 blend tree
- Ha de tenir mínim 1 capa extra d'animació.

 **(extra)** Blend tree 2D directional: per fer el moviment del personatge.

- Crear un script que controli el moviment del personatge.
  - Aquest control ha de manegar les diferents animacions i capes
  - Ha de poder realitzar els verbs que s'esmenten anteriorment.
  - Control del moviment del personatge.
- Emprar el Character Controller, i utilitzar la funció CharacterController.move()
- Emprar el Input Manager (el fet servit fins ara) amb els controls de teclat (WASD i fletxes) i ratolí. Teclat moviment, ratolí direcció.
Programar una tecla que executi l'animació de celebració.

- **(extra)** Utilització de Input System per als controls del personatge
- **(extra)** Afegir un comandament tipus Xbox o PlayStation per a controlar el personatge.

### R1.1.2 - Camera setting: Càmera i moments cinemàtics

         Per realitzar aquesta segona part s'ha de crear una escena nova amb l'attrezzo igual a l'anterior escena i el personatge amb la càmera implementada. L'escena tindrà nom de "Test-R1.1.2".

- Crear una càmera en 3a persona:
  - Aquesta càmera ha d’estar creada amb l’eina cinemachine.
  - La càmera rotarà sense passar a la part frontal del personatge.
  - Quan el personatge porta l'arma desenfundada, la càmera estarà més a prop del personatge ajudant al jugador a apuntar millor.
  - La càmera ha de tenir col·lisió amb els elements d’escena.

- Moments cinemàtics:
  - En prémer el botó de celebració, canvia la càmera a una altra càmera virtual en la qual es pugui veure aquesta animació.
  - **(extra)** Animar altres elements de l'escena: afegir sistemes de partícules, animar altres elements, etc.
  - **(extra)** Utilitza el PlayableDirector i Timeline per realitzar aquesta cinemàtica.
