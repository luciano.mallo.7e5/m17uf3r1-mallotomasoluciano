using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;

public class WeaponController : MonoBehaviour
{
    [Header("Input Controller")]
    private InputSystemController inputSystemController;

    private Transform spawnBulletPosition;
    #region
    [Header("General")]
    public Transform CameraPlayerTransForm;
    public LayerMask hittableLayers;

    float mouseSense = 1;
    float aimSmoothSpeed = 20;
    float xAxis, yAxis;
    public Transform aimPos;
    private bool readyToAim;
    private float waitForCamera = 3f;
    #endregion
    [Header("Shoot Parameters")]
    private float _fireRate = 200f;
    private float _nextTimeToShoot = 0;
    private int _actualAmmo = 200;
    private bool _isReloading;
    private int _totalAmmo;
    private float _reloadTime;
    [SerializeField] private Transform prefabBullet;
    private void Awake()
    {
        inputSystemController = GameObject.Find("PlayerModel").GetComponent<InputSystemController>();
        spawnBulletPosition = GameObject.Find("SpawnBullet").GetComponent<Transform>();
        CameraPlayerTransForm = Camera.main.transform;

    }

    private void Update()
    {

        if (inputSystemController._aim)
        {
            StartCoroutine(WaitForCameraTransition());
            if (readyToAim)
            {
                Aim();
                if (inputSystemController._fire)
                {
                    HandleShoot();
                    
                }
            }
        }
        else
        {
            readyToAim = false;
        }
    }

    IEnumerator WaitForCameraTransition()
    {
        //Wait for camera transition
        yield return new WaitForSeconds(waitForCamera);
        readyToAim = true;
    }

    private void Aim()
    {
        xAxis += inputSystemController._look.x * mouseSense;
        yAxis -= inputSystemController._look.y * mouseSense;
        yAxis = Mathf.Clamp(yAxis, -80, 80);

        Transform playerRot = GameObject.FindWithTag("Player").GetComponent<Transform>();
        playerRot.rotation = Quaternion.Euler(0, Mathf.Lerp(transform.rotation.y, xAxis, aimSmoothSpeed), 0);

        Vector2 screenCentre = new Vector2(Screen.width / 2, Screen.height / 2);
        Ray ray = Camera.main.ScreenPointToRay(screenCentre);

        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, hittableLayers))
            aimPos.position = Vector3.Lerp(aimPos.position, hit.point, aimSmoothSpeed * Time.deltaTime);

        GameObject.FindWithTag("Player").GetComponentInParent<Transform>().LookAt(aimPos);
    }
    void Fire()
    {


        spawnBulletPosition.LookAt(aimPos);
        GameObject bullet = Instantiate(prefabBullet.gameObject, spawnBulletPosition.position, spawnBulletPosition.rotation);
        inputSystemController._fire = false;

    }

    public void HandleShoot()
    {

        _nextTimeToShoot += Time.deltaTime;

        if (_actualAmmo > 0 && !_isReloading)
        {

            if (Time.time > _nextTimeToShoot)
            {

                _actualAmmo--;
                if (prefabBullet.gameObject != null)
                {

                    Fire();
                }
                _nextTimeToShoot = Time.time + 1 / _fireRate;

            }
        }
        else
        {
            //Reload();
        }

    }
    public void Reload()
    {
        if (_totalAmmo > 0)
        {
            StartCoroutine(Reloading());
        }
        else
        {
            return;
        }

    }
    IEnumerator Reloading()
    {
        // Reloading
        _isReloading = true;

        yield return new WaitForSeconds(_reloadTime);

        _isReloading = false;


    }
}

