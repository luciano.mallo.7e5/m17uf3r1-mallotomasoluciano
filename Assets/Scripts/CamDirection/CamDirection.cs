using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamDirection : MonoBehaviour
{
    public Camera mainCamera;
    private Vector3 cameraForward;
    private Vector3 cameraRight;
    private void Awake()
    {
        mainCamera = Camera.main;
    }
    private void Update()
    {
        cameraForward = mainCamera.transform.forward;
        cameraRight = mainCamera.transform.right;
        cameraForward.y = 0;
        cameraRight.y = 0;
        cameraForward = cameraForward.normalized;
        cameraRight = cameraRight.normalized;

    }


    public Vector3 CalculateRelativeMovementOfPlayerFromCamera(Vector3 playerInput)
    {
        Vector3 relativeMovOfplayer;

        relativeMovOfplayer = playerInput.x * cameraRight + playerInput.z * cameraForward;

        return relativeMovOfplayer;
    }
}
